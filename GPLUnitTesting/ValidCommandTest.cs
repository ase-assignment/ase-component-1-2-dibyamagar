﻿/*using GPL_Application;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GPLUnitTesting
{
    [TestClass]
    public class ValidCommandTest
	{ 
		/// <summary>
		/// Tests whether an invalid command call is found.
		/// </summary>
		[TestMethod]
		public void Test_Invalid_Command_Call()
		{
			// Check whether an incorrect command error message is returned from ValidateCommand()
			CommandParser cmd = new CommandParser();

			string commandString = "IncorrectCommand";
			string parameterString = "parameter1, paramteter2";

			string errorMessage;
			bool actual = cmd.ValidateCommand(1, commandString, parameterString, out errorMessage);
			bool expected = false;

			Assert.AreEqual("IncorrectCommand is an invalid command. Please see 'help' for a list of commands.", errorMessage, "The wrong error message was returned.");
			Assert.AreEqual(expected, actual, "Incorrect command wasn't found.");
		}


		// Test whether an invalid colour is passed.
		[TestMethod]
		public void Test_Invalid_Colour_Validate_Colour()
		{
			// Pass an invalid colour to the ValidateColour method to ensure the correct errorMessage is returned
			CommandParser cmd = new CommandParser();

			string colourString = "Invalid";

			string errorMessage;
			bool actual = cmd.ValidateColour(colourString, out errorMessage);
			bool expected = false;

			Assert.AreEqual("'Invalid' is not a known colour.", errorMessage, "The wrong error message was returned.");
			Assert.AreEqual(expected, actual, "Incorrect parameter type not spotted.");
		}
	}
}

*/
