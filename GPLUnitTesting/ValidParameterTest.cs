﻿/*using GPL_Application;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GPLUnitTesting
{
    [TestClass]
    public class ValidParameterTest
    {
        /// <summary>
        /// This test method splits the user input into two based on spaces
        /// </summary>
        [TestMethod]
        public void TestSplitUserInput()
        {
            CommandParser cmd = new CommandParser();
            string userInput = " command parameters";

            string[] actual = cmd.SplitUserInput(userInput);
            string[] expected = { "command", "parameters" };

            CollectionAssert.AreEqual(actual, expected, "Failed to split the command.");
        }

        // Tests SplitParameters() passing an expected return value from SplitUserInput() and a comma split delimiter
        [TestMethod]
        public void TestSplitParameters()
        {
            CommandParser cmd = new CommandParser();

            string userInput = " parameter1, parameter2 ";
            string splitDelimiter = ",";

            string[] actual = cmd.SplitParameters(userInput, splitDelimiter);
            string[] expected = { "parameter1", "parameter2" };

            CollectionAssert.AreEqual(actual, expected);
        }

        // When passed a string to the ValidateInteger method, test if the correct error message is returned
        [TestMethod]
        public void TestInvalid_Parmeter_Validate_Integer()
        {
            CommandParser cmd = new CommandParser();

            string parameterString = "parameter1";

            string errorMessage;
            bool actual = cmd.ValidateInteger(parameterString, out errorMessage);
            bool expected = false;

            Assert.AreEqual("'parameter1' must be a positive integer.", errorMessage, "The error message was returned.");
            Assert.AreEqual(expected, actual, "Incorrect parameter type not found.");
        }

        /// <summary>
		///  Pass an invalid variable point to the ValidatePoint method Tests if a valid variable is found.
		/// </summary>
		[TestMethod]
        public void Test_Valid_Variable_Point_Validate_Point()
        {
            CommandParser cmd = new CommandParser();

            cmd.ExecuteCommand(null, "point1=100", "");

            string pointParameter = "point1 100";

            string errorMessage;
            bool actual = cmd.ValidatePoint(pointParameter, out errorMessage);
            bool expected = true;

            Assert.AreEqual("", errorMessage, "The wrong error message was returned.");
            Assert.AreEqual(expected, actual, "Incorrect parameter type not found.");
        }

    }
}

*/