﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_Application
{
    /// <summary>
    /// Abstract Class that implements Shape Interface, recieves values of shapes, sets color, fill and other parameters of that shape
    /// Calls a method draw to draw that particular shape
    /// </summary>
    public abstract class Shape : ShapeInterface
    {
        /// <summary>
        /// variable which stores the name of color to be used in pen to draw objects or shapes
        /// </summary>
        protected Color penColor;
        /// <summary>
        /// Stores boolean values, True when 'fill on' and False when 'fill off' used to fill the shapes to be drawn
        /// </summary>
        protected bool fill;
        /// <summary>
        /// Stores the values of x-coordinates and y-coordinates of the display panel
        /// </summary>
        protected int x , y;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Shape()
        {
        }

        /// <summary>
        /// Refers the value of 'x' to the current object of the method to set its current value
        /// </summary>
        /// <param name="x">Holds the value of current position of x-axis</param>
        public void setX(int x)
        {
            this.x = x;
        }

        /// <summary>
        /// Gets the current value of x-coordinate from the user
        /// <return>The current value of the current position of x-coordinate</return>
        /// </summary>
        public int getX()
        {
            return this.x;
        }

        /// <summary>
        /// Triggered when shape command along with parameters is typed in the application
        /// </summary>
        /// <param name="penColor">Holds the Color of pen which draws the circle</param>
        /// <param name="fill">Holds the boolean value of fill - which is true when fill is on or false otherwise</param>
        /// <param name="x">Holds the value for x-coordinate</param>
        /// <param name="y">Holds the value for y-coordinate</param>
        public Shape(Color penColor,bool fill, int x, int y)
        {
            this.penColor = penColor;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Sets the penColor, fill and list of parameters and refers their to the current object of the method
        /// </summary>
        /// <param name="penColor">Holds the Color of pen which draws the rectangle.</param>
        /// <param name="fill">Holds the boolean value of fill - which is true when fill is on or false otherwise</param>
        /// <param name="list">list that holds radius, length, height,etc. to be set for the shape</param>
        public virtual void set(Color penColor, bool fill, params int[] list)
        {
            this.penColor = penColor;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }

        /// <summary>
        /// Method which will be inherited by shapes classes to draw object 
        /// All derived class must be implement this method
        /// </summary>
        /// <param name="g"></param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// Sets Texture Full based on texture file 
        /// </summary>
        /// <param name="textureFile"></param>
        public virtual void SetTexture(string textureFile)
        {
        }
    }
}
