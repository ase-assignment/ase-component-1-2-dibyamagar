﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPL_Application
{
    //Impelements Abtract Class Shape
    class Line: Shape
    {
        int newX, newY;

        public Line():base()
        {
            newX = 0;
            newY = 0;
        }

        /// <summary>
        /// Creates a new line with the specified colour and x,y values.
        /// </summary>
        /// <param name="penColor"></param>
        /// <param name="fill"></param>
        /// <param name="currentX">Integer: The value of the current X.</param>
        /// <param name="currentY">Integer: The value of the current Y.</param>
        /// <param name="x">Integer: The value for the new X.</param>
        /// <param name="y">Integer: The value for the new Y.</param>
        public Line(Color penColor, bool fill, int currentX, int currentY, int x, int y):base(penColor, fill, currentX, currentY)
        {
            this.newX = x;
            this.newY = y;
        }

        /// <summary>
        /// Sets the colour and x,y values of the line.
        /// </summary>
        /// <param name="penColor"></param>
        /// <param name="fill"></param>
        /// <param name="list">A list of integers for the currentX, currentY, newX and newY values.</param>
        public override void set(Color penColor, bool fill, params int[] list)
        {
            try
            {
                base.set(penColor, fill, list[0], list[1]);
                this.newX = list[2];
                this.newY = list[3];
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            
        }

        /// <summary>
        /// Draws a line between two points.
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            try
            {
                //Object of Pen that helps in drawing shapes
                Pen pen = new Pen(penColor, 1);

                //Checks if fill is on and fills the shape of the ecllipse
                if (fill == true)
                {
                    //Object of solidbrush which creates brush to fill the shapes
                    SolidBrush brush = new SolidBrush(penColor);
                    g.DrawLine(pen, new Point(x, y), new Point(newX, newY));

                }
                g.DrawLine(pen, new Point(x, y), new Point(newX, newY));
              
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
           
        }
    }
}
