﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_Application
{
    class Polygon : Shape
    {
        //Integer array which holds the list of parameters containing points of the polygon
        int[] integerPoints;
        List<Point> points = new List<Point>();
        public Polygon()
        {
        }

        /// <summary>
        /// overrides the set method of Shape class, sets the color, fill, x,y coordinates and sides of Polygon
        /// </summary>
        /// <param name="penColor"></param>
        /// <param name="fill"></param>
        /// <param name="list">list of integers for the currentX, currentY, x and y values to draw a polygon.</param>
        public override void set(Color penColor, bool fill, params int[] list)
        {
            base.set(penColor,fill, list[0], list[1]);
            integerPoints = list.Skip(2).ToArray();
        }

        public override void draw(Graphics g)
        {
            // Iterates the loop to add new point into integerPoints array.
            for (int i =0; i <integerPoints.Length; i+= 2)
            {
                points.Add(new Point(integerPoints[i], integerPoints[i+1]));
            }

            Pen pen = new Pen(penColor, 1);
            if (fill == true)
            {
                // Object of SolidBrush which creates brush to fill the polygon
                SolidBrush brush = new SolidBrush(penColor);

                //Fills the triangle at the given point within the area of triangle.
                g.FillPolygon(brush, points.ToArray());
            }

            // Draws the triangle at the given point with the points in the point array: trianglePoints.
            g.DrawPolygon(pen, points.ToArray());
        }
    }
}
