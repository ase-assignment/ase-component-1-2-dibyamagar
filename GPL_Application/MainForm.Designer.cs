﻿
namespace GPL_Application
{
    /// <summary>
    /// 
    /// </summary>
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userGuidelinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DrawingPanel = new System.Windows.Forms.Panel();
            this.CodeInputTabControl = new System.Windows.Forms.TabControl();
            this.SingleLineInputTab = new System.Windows.Forms.TabPage();
            this.checkSyntaxButton = new System.Windows.Forms.Button();
            this.cmdLineInput = new System.Windows.Forms.TextBox();
            this.outputConsole = new System.Windows.Forms.RichTextBox();
            this.programWindow = new System.Windows.Forms.TextBox();
            this.DisplayPictureBox = new System.Windows.Forms.PictureBox();
            this.HelpPanel = new System.Windows.Forms.Panel();
            this.HelpGridView = new System.Windows.Forms.DataGridView();
            this.CommandColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParametersColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            this.DrawingPanel.SuspendLayout();
            this.CodeInputTabControl.SuspendLayout();
            this.SingleLineInputTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayPictureBox)).BeginInit();
            this.HelpPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HelpGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1252, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(54, 29);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(232, 34);
            this.openToolStripMenuItem.Text = "Open Program";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::GPL_Application.Properties.Resources.icons8_save_32;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(232, 34);
            this.saveToolStripMenuItem.Text = "Save Program";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(232, 34);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.infoToolStripMenuItem,
            this.userGuidelinesToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(78, 29);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(235, 34);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(235, 34);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // userGuidelinesToolStripMenuItem
            // 
            this.userGuidelinesToolStripMenuItem.Name = "userGuidelinesToolStripMenuItem";
            this.userGuidelinesToolStripMenuItem.Size = new System.Drawing.Size(235, 34);
            this.userGuidelinesToolStripMenuItem.Text = "User Guidelines";
            // 
            // DrawingPanel
            // 
            this.DrawingPanel.Controls.Add(this.CodeInputTabControl);
            this.DrawingPanel.Controls.Add(this.DisplayPictureBox);
            this.DrawingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DrawingPanel.Location = new System.Drawing.Point(0, 33);
            this.DrawingPanel.Name = "DrawingPanel";
            this.DrawingPanel.Size = new System.Drawing.Size(1252, 601);
            this.DrawingPanel.TabIndex = 15;
            // 
            // CodeInputTabControl
            // 
            this.CodeInputTabControl.Controls.Add(this.SingleLineInputTab);
            this.CodeInputTabControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.CodeInputTabControl.Location = new System.Drawing.Point(537, 0);
            this.CodeInputTabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CodeInputTabControl.Name = "CodeInputTabControl";
            this.CodeInputTabControl.SelectedIndex = 0;
            this.CodeInputTabControl.Size = new System.Drawing.Size(715, 601);
            this.CodeInputTabControl.TabIndex = 15;
            // 
            // SingleLineInputTab
            // 
            this.SingleLineInputTab.Controls.Add(this.checkSyntaxButton);
            this.SingleLineInputTab.Controls.Add(this.cmdLineInput);
            this.SingleLineInputTab.Controls.Add(this.outputConsole);
            this.SingleLineInputTab.Controls.Add(this.programWindow);
            this.SingleLineInputTab.Location = new System.Drawing.Point(4, 29);
            this.SingleLineInputTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SingleLineInputTab.Name = "SingleLineInputTab";
            this.SingleLineInputTab.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SingleLineInputTab.Size = new System.Drawing.Size(707, 568);
            this.SingleLineInputTab.TabIndex = 0;
            this.SingleLineInputTab.Text = "Type your Program Code Here";
            this.SingleLineInputTab.UseVisualStyleBackColor = true;
            // 
            // checkSyntaxButton
            // 
            this.checkSyntaxButton.BackColor = System.Drawing.SystemColors.HighlightText;
            this.checkSyntaxButton.Location = new System.Drawing.Point(558, 2);
            this.checkSyntaxButton.Name = "checkSyntaxButton";
            this.checkSyntaxButton.Size = new System.Drawing.Size(146, 33);
            this.checkSyntaxButton.TabIndex = 7;
            this.checkSyntaxButton.Text = "Check Syntax";
            this.checkSyntaxButton.UseVisualStyleBackColor = false;
            this.checkSyntaxButton.Click += new System.EventHandler(this.checkSyntaxButton_Click);
            // 
            // cmdLineInput
            // 
            this.cmdLineInput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cmdLineInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLineInput.Location = new System.Drawing.Point(3, 279);
            this.cmdLineInput.Name = "cmdLineInput";
            this.cmdLineInput.Size = new System.Drawing.Size(701, 35);
            this.cmdLineInput.TabIndex = 3;
           
            this.cmdLineInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdLineInput_KeyDown);
            // 
            // outputConsole
            // 
            this.outputConsole.BackColor = System.Drawing.SystemColors.ControlText;
            this.outputConsole.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.outputConsole.Enabled = false;
            this.outputConsole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputConsole.ForeColor = System.Drawing.Color.White;
            this.outputConsole.Location = new System.Drawing.Point(3, 314);
            this.outputConsole.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputConsole.Name = "outputConsole";
            this.outputConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.outputConsole.Size = new System.Drawing.Size(701, 252);
            this.outputConsole.TabIndex = 1;
            this.outputConsole.Text = "";
            // 
            // programWindow
            // 
            this.programWindow.BackColor = System.Drawing.SystemColors.ControlText;
            this.programWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.programWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programWindow.ForeColor = System.Drawing.Color.White;
            this.programWindow.Location = new System.Drawing.Point(3, 2);
            this.programWindow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.programWindow.Multiline = true;
            this.programWindow.Name = "programWindow";
            this.programWindow.Size = new System.Drawing.Size(701, 564);
            this.programWindow.TabIndex = 0;
         
            // 
            // DisplayPictureBox
            // 
            this.DisplayPictureBox.BackColor = System.Drawing.Color.White;
            this.DisplayPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DisplayPictureBox.Location = new System.Drawing.Point(0, 0);
            this.DisplayPictureBox.Name = "DisplayPictureBox";
            this.DisplayPictureBox.Size = new System.Drawing.Size(1252, 601);
            this.DisplayPictureBox.TabIndex = 16;
            this.DisplayPictureBox.TabStop = false;
            this.DisplayPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.DisplayPictureBoxPaint);
            // 
            // HelpPanel
            // 
            this.HelpPanel.Controls.Add(this.HelpGridView);
            this.HelpPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.HelpPanel.Location = new System.Drawing.Point(0, 539);
            this.HelpPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HelpPanel.Name = "HelpPanel";
            this.HelpPanel.Size = new System.Drawing.Size(1252, 95);
            this.HelpPanel.TabIndex = 16;
            this.HelpPanel.Visible = false;
            // 
            // HelpGridView
            // 
            this.HelpGridView.AllowUserToAddRows = false;
            this.HelpGridView.AllowUserToDeleteRows = false;
            this.HelpGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HelpGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.HelpGridView.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.HelpGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HelpGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CommandColumn,
            this.ParametersColumn,
            this.DescriptionColumn});
            this.HelpGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HelpGridView.Location = new System.Drawing.Point(0, 0);
            this.HelpGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HelpGridView.Name = "HelpGridView";
            this.HelpGridView.ReadOnly = true;
            this.HelpGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.HelpGridView.RowTemplate.Height = 24;
            this.HelpGridView.Size = new System.Drawing.Size(1252, 95);
            this.HelpGridView.TabIndex = 0;
            // 
            // CommandColumn
            // 
            this.CommandColumn.HeaderText = "Command";
            this.CommandColumn.MinimumWidth = 8;
            this.CommandColumn.Name = "CommandColumn";
            this.CommandColumn.ReadOnly = true;
            // 
            // ParametersColumn
            // 
            this.ParametersColumn.HeaderText = "Parameters";
            this.ParametersColumn.MinimumWidth = 8;
            this.ParametersColumn.Name = "ParametersColumn";
            this.ParametersColumn.ReadOnly = true;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.MinimumWidth = 8;
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1252, 634);
            this.Controls.Add(this.HelpPanel);
            this.Controls.Add(this.DrawingPanel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graphical Programming Language Application";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.DrawingPanel.ResumeLayout(false);
            this.CodeInputTabControl.ResumeLayout(false);
            this.SingleLineInputTab.ResumeLayout(false);
            this.SingleLineInputTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayPictureBox)).EndInit();
            this.HelpPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HelpGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Panel DrawingPanel;
        private System.Windows.Forms.TabControl CodeInputTabControl;
        private System.Windows.Forms.Panel HelpPanel;
        private System.Windows.Forms.DataGridView HelpGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommandColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParametersColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userGuidelinesToolStripMenuItem;
        private System.Windows.Forms.TabPage SingleLineInputTab;
        private System.Windows.Forms.Button checkSyntaxButton;
        private System.Windows.Forms.TextBox cmdLineInput;
        private System.Windows.Forms.RichTextBox outputConsole;
        private System.Windows.Forms.TextBox programWindow;
        private System.Windows.Forms.PictureBox DisplayPictureBox;
    }
}

