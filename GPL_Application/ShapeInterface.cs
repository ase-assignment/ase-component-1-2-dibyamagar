﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_Application
{
    interface ShapeInterface
    {
        /// <summary>
        /// Interface Method to be inherited by Shapes class and draw the object in display panel
        /// </summary>
        /// <param name="g"></param>
        void draw(Graphics g);

        /// <summary>
        ///Sets the penColor, fill and list of parameters and refers their to the current object of the method
        /// </summary>
        /// <param name="penColor">Holds the Color of pen which draws the rectangle.</param>
        /// <param name="fill">Holds the boolean value of fill - which is true when fill is on or false otherwise</param>
        /// <param name="list">list that holds radius, length, height,etc. to be set for the shape</param>
        void set(Color penColor, bool fill, params int[] list);

    }
}
