﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace GPL_Application
{
    /// <summary>
    /// This class holds the commands of circle
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// Getting Values of x, y position and radius of circle
        /// </summary>
        int radius;

        /// <summary>
        /// base keyword is used to access members of the base class from within a derived class
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// Overrides the set method of Shape class
        /// Sets the Color, fill value, (x,y) of cursor, and radiysof circle. 
        /// </summary>
        /// <param name="penColor">Holds the Color of pen which draws the shape.</param>
        /// <param name="fill">Holds the boolean value of fill - which is true when fill is on or false otherwise</param>
        /// <param name="list">list that holds x,y,radius to be set for the shape</param>
        public override void set(Color penColor, bool fill, params int[] list)
        {
            try
            {
                base.set(penColor, fill, list[0], list[1]);
                this.radius = list[2];
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Method to Draw the circle in picturebox
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            try
            {
                //Object of Pen that helps in drawing shapes
                Pen pen = new Pen(penColor, 2);

                //Checks if fill is on and fills the shape of the ecllipse
                if(fill == true)
                {
                    //Object of solidbrush which creates brush to fill the shapes
                    SolidBrush brush = new SolidBrush(penColor);

                    g.FillEllipse(brush, x - radius, y - radius, radius * 2, radius * 2);
                }
                g.DrawEllipse(pen, x-radius, y-radius, radius * 2, radius * 2);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
