﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace GPL_Application
{
    /// <summary>
    /// Shape factory to return the requested shape.
    /// </summary>
    public class ShapeFactory
    {
        /// <summary>
        /// Checking the Shape Type and Returning Shapes
        ///Creates object of Rectangle/Triangle/Circle class if the passed parameter matches the Shape name. 
        /// </summary>
        /// <param name="shapeType">Holds the name of shape of which object is to be created.</param>
        /// <returns>Shape type to be returned to the user</returns>
        public Shape GetShape(String shapeType)
        {
            //Trims the spaces before and after the text and changes the alphabets to uppercase. 
            shapeType = shapeType.ToUpper().Trim();

            //Checks the string to different shape names. Returns their object if true. 
            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();
            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else if (shapeType == "SQUARE")
            {
                return new Rectangle();
            }
            else if (shapeType == "DRAWTO")
            {
                return new Line();
            }
            else if (shapeType == "POLYGON")
            {
                return new Polygon();
            }
            else
            {
                //hrows error if the string in parameter is not matched with any of the shapes
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + "does not exist currently.");
                throw argEx;
            }

        }
    }
}
