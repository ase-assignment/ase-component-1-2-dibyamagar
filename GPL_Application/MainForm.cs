﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections;

namespace GPL_Application
{
    /// <summary>
    /// Main form of the GPL Application
    /// </summary>
    public partial class MainForm : Form
    {
        Bitmap bitmapCanvas = new Bitmap(500, 500);
        ArrayList shapelist = new ArrayList();
        /// <summary>
        /// Object of <see cref="CommandParser"/>: Allows <see cref="MainForm"/> to use the methods and codes of <see cref="CommandParser"/> class.
        /// </summary>
        CommandParser cmd;
        /// <summary>
        /// Stores Boolean for Syntax Checking Button
        /// </summary>
        bool syntaxButton = false;
        string previousCommand;

        /// <summary>
        /// Default Constructor: Initializes components which displays the Form and Graphical contents of the application.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            //Draws the contents from the bitmap into the application's pictureBox
            cmd = new CommandParser(Graphics.FromImage(bitmapCanvas));
        }

        /// <summary>
        /// Method: <list type="bullet">Checks the value stored in the parameter and calls <see cref="CommandParser.ClearScreen"/> if 'clear' and <see cref="CommandParser.ResetPen"/> if 'reset'</list>
        ///         <list type="bullet">Retrieves multiple line of code from programWindow and calls <see cref="CommandParser.commandReader"/> if 'run'. </list>
        ///         <list type="bullet">Else, calls <see cref="CommandParser.commandReader"/> to check if the code is valid and performs tasks otherwise.</list>
        ///</summary>
        /// <param name="enteredCode">The command retrieved from the Command Line Window of the application</param>
        public void commandReader(String enteredCode)
        {
            /*
             * Checks the string written in the String: code and calls appropriate methods accordingly.
             */
            if (enteredCode.Equals("clear"))
                // Calls clearScreen method of CommandParser class which clears the drawing window pictureBox of the application
                cmd.ClearScreen();

            else if (enteredCode.Equals("reset"))
                // Calls ResetPen method of CommandParser class which changes the position of pen to (0,0) coordinates.
                cmd.ResetPen();

            else if (enteredCode.Equals("run"))
            {
                // Creates an array of string, retrieves the text from programWindow and stores the lines. 
                string[] multilineCodes = programWindow.Lines;

                // Index to count the line number of the text
                int index = 1;

                /*
                 * Iterates to retrive each line of text that was initially written in the programWindow and 
                 * calls commandReader method, of CommandParser class, along with each line of text.
                */
                foreach (String line in multilineCodes)
                {
                    // Increases index every time the line is changed (the loop is iterated).
                    int counter = index++;

                    // Calls commandReader method, of CommandParser class, and sends the line retrieved, counter and syntaxButton's boolean value as the parameter.
                    cmd.commandReader(line, counter, syntaxButton);
                }
            }

            else
            {
                // Calls commandReader method, of CommandParser class, and sends the line retrieved, 1 and syntaxButton's boolean value as the parameter.
                cmd.commandReader(enteredCode, 1, syntaxButton);
            }
        }

        private void cmdLineInput_KeyDown(object sender, KeyEventArgs e)
        {
            /*
             * Checks if the key pressed is Enter and performs tasks, if TRUE.
             */
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                // Sets the text of outputConsole to null so that it does not repeat previously presented Error Messages.
                outputConsole.Text = "";

                // Sets the color of the text to be displayed in error console, to 'red'.
                outputConsole.ForeColor = Color.Red;

                // Calls clearErrorList() method of CommandParser class.
                cmd.clearErrorList();

                // Calls clearDictionary() method of CommandParser class.
                cmd.clearDictionary();

                // Retrieves and Stores the text written in Command Line Window
                String command = cmdLineInput.Text;

                // Trims the spaces before and after the text and changes the alphabets to lowercase.
                command = command.Trim().ToLower();

                // Calls commandReader method and sends the command retrieved as the parameter.
                commandReader(command);

                /*
                 * Iterates to retrieve the contents of ArrayList: errorList created in the CommandParser class.
                 */
                foreach (String eachError in CommandParser.errorList)
                {
                    // Retrieves each item of the ArrayList  and appends it to outputConsole of the application.
                    outputConsole.AppendText(eachError + "\n\n");
                }

                // Sets the text of the commandLineWindow to null so that the contents written is cleared everytime when 'Enter' is pressed.
                cmdLineInput.Text = "";
                Refresh();
                
            }

            else if (e.KeyCode == Keys.Up)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                if (!String.IsNullOrWhiteSpace(previousCommand))
                {
                    this.cmdLineInput.Text = previousCommand;
                }
            }
        }

        private void DisplayPictureBoxPaint(object sender, PaintEventArgs e)
        {
            // Creates Graphics object g to paint or draw contents in the application.
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(bitmapCanvas, 0, 0);
        }

        /// <summary>
        /// The function loads the text file from the desired location.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Title = "Browse file from specified folder";
            openFileDialog1.InitialDirectory = "E:\\";
            openFileDialog1.Filter = "TXT files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.Filter = "DOCX files (*.docx)|*.docx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            //Browse .txt file from computer             
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            // Insert code to read the stream here.                        
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
                //displays the text inside the file on TextBox named as txtInput                
                programWindow.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }

        /// <summary>
        /// The function saves the text file in the desired location.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "TXT files (*.txt)|*.txt|All files (*.*)|*.*";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter write = new StreamWriter(File.Create(save.FileName));
                write.WriteLine(programWindow.Text);
                write.Close();
                MessageBox.Show("File Successfully Save In Your Computer");
            }
        }

        /// <summary>
        /// Closes the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// This function closes the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

     
        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HelpGridView.Rows.Clear();
            //Populating date for Help Table
            string[][] rows = new string[][] {
                new string[] { "Run", "<String: File path to a file to run>", "Run commands from a specified file." },
                new string[] { "Clear", "", "Clears the current shapes on the CommandParser." },
                new string[] { "Loop", "<Integer: Number of times to loop>; <Shape Command: The shape to draw>; ...", "Single line loop which based on the first parameter will iterate the commands." },
                new string[] { "Loop", "<Integer: Number of times to loop> NEW LINE <Shape Command: The shape to draw> NEW LINE EndLoop", "Draws a square in the specified colour." },
                new string[] { "If", "<Integer> <Conditional: ==, <, >> <Integer>; <Shape Command; The shape to draw>; ...", "If the conditional is true the shape commands will be ran." },
                new string[] { "If", "<Integer> <Conditional: ==, <, >> <Integer>; NEW LINE <Shape Command; The shape to draw>; NEW LINE ENDIF", "If the conditional is true the shape commands will be ran." },
                new string[] { "MoveTo", "<Colour: Shape colour>, <Integer: X value>, <Integer: Y value>", "If the pen is down MoveTo will draw a line to the specified X and Y." },
                new string[] { "MoveTo", "<Integer: X value>, <Integer: Y value>", "If the pen is up MoveTo will move the reference to the specified x and y." },
                new string[] { "Square", "<Colour: Shape colour>, <Integer: Width>", "Draws a square in the specified colour." },
                new string[] { "Circle", "<Colour: Shape colour>, <Integer: Diameter>", "Draws a circle in the specified colour." },
                new string[] { "Rectangle", "<Colour: Shape colour>, <Integer: Width>, <Integer: Height>", "Draws a rectangle in the specified colour." },
                new string[] { "RectangleTexture", "<Colour: Shape colour>, <FilePath: To the image>, <Integer: Width>, <Integer: Height>", "Draws a rectangle with the colour as an outline and filled as the image." },
                new string[] { "Triangle", "<Colour: Shape colour>, <Integer: Point 1x> <Integer: Point1y>, <Integer: Point2a> <Integer: Point2b>, <Integer: Point3a> <Integer: Point3b>", "Draws a triangle in the specified colour." },
                new string[] { "Polygon", "<Colour: Shape colour>, <Integer: Point 1x> <Integer: Point1y>, <Integer: Point2a> <Integer: Point2b>, <Integer: Point3a> <Integer: Point3b>", "Draws a polygon in the specified colour." },
                new string[] { "Repeat", "<Integer: Number of repeats>, <Shape Command: The shape to draw>, <Color: shape colour>, <Optional: Either + or ->, <Integer: Starting point>", "Incrementally repeats a shape command and either + or - the starting point each repeat." }
            };

            for (int i = 0; i < rows.Length; i++)
            {
                this.HelpGridView.Rows.Add(rows[i]);
            }

            this.HelpPanel.Visible = true;
            this.DrawingPanel.Visible = false;
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HelpPanel.Visible = false;
            this.DrawingPanel.Visible = true;
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String message = "This application is developed by Dibya Rani Saru Magar with the motive to produce a simplified programming " +
                "environment for learning concepts of programming.\n\nApplication Programmer: " +
                "\nDibya Rani Saru Magar\nUniversity ID: 7275329\nThe British College\n\n" +
                "Copyright © 2021. All rights reserved.";

            // Holds the title of the message Box
            String title = "Info - Graphical Programming Language Application";

            // Displays Message box along with the title and message
            MessageBox.Show(message, title);
        }

        private void checkSyntaxButton_Click(object sender, EventArgs e)
        {
            // Changes the boolean value of syntaxButton to 'true' which checks if the button was pressed.
            syntaxButton = true;

            // Sets the text of outputConsole to null so that it does not repeat previously presented Error Messages.
            outputConsole.Text = "";

            // Sets the color of the text to be displayed in outputConsole, to 'red'.
            outputConsole.ForeColor = Color.Red;

            // Calls clearErrorList() method of CommandParser class.
            cmd.clearErrorList();

            // Calls clearDictionary() method of CommandParser class.
            cmd.clearDictionary();

            // Creates an array of string, retrieves the text from programWindow and stores the lines. 
            String[] multilineCodes = programWindow.Lines;

            // Index to count the line number of the text
            int index = 1;

            /*
             * Iterates to retrive each line of text that was initially written in the programWindow and 
             * calls commandReader method, of CommandParser class, along with each line of text.
             */
            foreach (String line in multilineCodes)
            {
                // Increases index every time the line is changed (the loop is iterated).
                int counter = index++;

                // Calls commandReader method, of CommandParser class, and sends the line retrieved, counter and syntaxButton's boolean value as the parameter.
                cmd.commandReader(line, counter, syntaxButton);
            }

            // Calls resetPen method of CommandParser class which resets the x and y coordinates of the pen in the displayCommandParser pictureBox of the application to return to default state after syntax check
            cmd.ResetPen();

            // Calls resetPen method of CommandParser class
            // cmd.ClearScreen();

            // Changes the color of the pen to black to return to default after syntax check
            cmd.penColor = Color.Black;

            // Changes the fill value to false to return to default state after syntax check
            cmd.fill = false;

            /*
             * Iterates to retrieve the contents of ArrayList: errorList created in the CommandParser class.
             */
            foreach (String eachError in CommandParser.errorList)
            {
                // Retrieves each item of the ArrayList and appends it to outputConsole of the application.
                outputConsole.AppendText(eachError + "\n\n");
            }

            /*
             * Checks if the ArrayList: errorList has any data and display proceed if there is no data. 
             */
            if (CommandParser.errorList.Count == 0)
            {
                // Sets the color of the text to be displayed in outputConsole, to 'green'.
                outputConsole.ForeColor = Color.Green;

                // Appends the message to proceed to outputConsole of the application.
                outputConsole.AppendText("The program does not have any error! \n Type Run command to execute the codes.");

            }

            // Changes the boolean value of syntaxButton to 'false' as all the tasks to be performed by syntaxButton has been already done. 
            syntaxButton = false;
        }
    }
}
