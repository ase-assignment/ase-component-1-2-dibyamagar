﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_Application.Validations
{
    /// <summary>
    /// This class contains methods that check if the shape user entered exists
    /// </summary>
    public class ShapeValidator
    {
        private string baseValue;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="haystack"></param>
        /// <returns></returns>
        public Boolean CheckString(string haystack)
        {
            return this.baseValue.ToLower().Equals(haystack);
        }

        /// <summary>
        /// Checks 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boolean validate(string name)
        {
            this.baseValue = name;
            if (CheckString("circle") || CheckString("rectangle") || CheckString("triangle"))
            {
                return true;
            }
            return false;
        }
    }
}
