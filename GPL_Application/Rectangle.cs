﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace GPL_Application
{
    /// <summary>
    /// This class holds commands to draw Rectangle
    /// </summary>
    public class Rectangle : Shape
    {
        TextureBrush tbrush;
        /// <summary>
        /// Holds the integer value of height and width of the rectangle which is obtained from parameter list.
        /// </summary>
        int height, width;

        /// <summary>
        /// Default Constructor
        public Rectangle() : base()
        {

        }

        /// <summary>
        /// Overrides the set method of Shape class
        /// Sets the Color, fill value, (x,y) of cursor, and length, breadth of rectangle. 
        /// </summary>
        /// <param name="penColor">Holds the Color of pen which draws the rectangle.</param>
        /// <param name="fill">Holds the boolean value of fill - which is true when fill is on or false otherwise</param>
        /// <param name="list">list that holds radius, length, height,etc. to be set for the shape</param>
        public override void set(Color penColor, bool fill, params int[] list)
        {
            try
            {
                base.set(penColor, fill, list[0], list[1]);
                this.width = list[2];
                this.height = list[3];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }


        /// <summary>
        /// Method to draw shapes in the display panel based on the width and height
        /// </summary>
        /// <param name="g">he graphics panel of where to draw the rectangle.</param>
        public override void draw(Graphics g)
        {
            try
            {
                //Object of Pen that helps in drawing shapes
                Pen pen = new Pen(penColor, 1);

                //Checks if fill is on and fills the shape of the rectangle
                if (fill == true)
                {
                    //Object of solidbrush which creates brush to fill the shapes
                    SolidBrush brush = new SolidBrush(penColor);

                    g.FillRectangle(brush,x,y,height, width);
                }
                g.DrawRectangle(pen, x, y, height, width);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Sets Texture of the Rectangle
        /// </summary>
        /// <param name="textureFile"></param>
        public override void SetTexture(string textureFile)
        {
            Image image = new Bitmap(textureFile);
            tbrush = new TextureBrush(image);
        }
    }
}
